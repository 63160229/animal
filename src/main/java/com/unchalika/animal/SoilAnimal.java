/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unchalika.animal;

/**
 *
 * @author Tuf Gaming
 */
public abstract class SoilAnimal extends Animal {

    public SoilAnimal(String name, int numberOfLeg) {
        super(name, numberOfLeg);
    }
    public abstract void hole();
}

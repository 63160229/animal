/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unchalika.animal;

/**
 *
 * @author Tuf Gaming
 */
public class TestAnimal {

    public static void main(String[] args) {
        Human h1 = new Human("Dang");
        h1.eat();
        h1.walk();
        h1.run();
        h1.speak();
        h1.sleep();
        System.out.println("h1 is animal? " + (h1 instanceof Animal));
        System.out.println("h1 is animal? " + (h1 instanceof LandAnimal));

        Animal a1 = h1;
        System.out.println("a1 is animal? " + (a1 instanceof LandAnimal));
        System.out.println("a1 is animal? " + (a1 instanceof Reptile));
        System.out.println("-------------------------------");
        Cat c1 = new Cat("Zom");
        c1.eat();
        c1.walk();
        c1.run();
        c1.speak();
        c1.sleep();
        System.out.println("-------------------------------");
        Dog d1 = new Dog("Pluto");
        d1.eat();
        d1.walk();
        d1.run();
        d1.speak();
        d1.sleep();
        System.out.println("-------------------------------");
        Crocodile cd1 = new Crocodile("Dum");
        cd1.eat();
        cd1.walk();
        cd1.crawl();
        cd1.speak();
        cd1.sleep();
        System.out.println("-------------------------------");
        Snake s1 = new Snake("Luna");
        s1.eat();
        s1.walk();
        s1.crawl();
        s1.speak();
        s1.sleep();
        System.out.println("-------------------------------");
        Crab cb1 = new Crab("Kam");
        cb1.eat();
        cb1.walk();
        cb1.swim();
        cb1.speak();
        cb1.sleep();
        System.out.println("-------------------------------");
        Fish f1 = new Fish("Tong");
        f1.eat();
        f1.walk();
        f1.swim();
        f1.speak();
        f1.sleep();
        System.out.println("-------------------------------");
        Bat b1 = new Bat("Jeji");
        b1.eat();
        b1.walk();
        b1.fly();
        b1.speak();
        b1.sleep();
        System.out.println("-------------------------------");
        Bird bd1 = new Bird("Boy");
        bd1.eat();
        bd1.walk();
        bd1.fly();
        bd1.speak();
        bd1.sleep();
        System.out.println("-------------------------------");
        Earthworm e1 = new Earthworm("Bobby");
        e1.eat();
        e1.walk();
        e1.hole();
        e1.speak();
        e1.sleep();
        System.out.println("-------------------------------");

    }

}
